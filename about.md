---
title: About
layout: page
---
![Profile Image]({% if site.external-image %}{{ site.picture }}{% else %}{{ site.url }}/{{ site.picture }}{% endif %})

Hi! I go by mikwee online since 2014. I'm Jewish, Israeli, and
autistic. I'm 17, and I enjoy a lot of things, as seen below. Oh, and
I'm also a brony. Thanks for coming by!

## Intersts

- ✏️ Animation (Especially My Little Pony G4)
- 🎮 Gaming
- ✍️ Writing
- 🎵 Music
- ✡️ Religion (from a secular Jewish perspective)
- 📐 Design
- 💻 Programming
- 🧦 Center-left poltiics

## Skills

- React
- CSS
- Git
- Dart
- Flutter
- Ruby
- Writing

## Accounts that aren't linked in the homepage

- [No Context Israeli Culture](https://twitter.com/NCILCulture)
- [Musicboard](https://musicboard.app/mikwee)
- [Letterboxd](https://letterboxd.com/mikwee)
- Discord: mikwee#1885
- Matrix: @mikwee:matrix.org
- Revolt: @mikwee